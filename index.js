'use strict';

const axios = require('axios');
const https = require('https');
const Validator = require('jsonschema').Validator;
const sshpk = require('sshpk');

const optionsSchema = {
  'type': 'object',
  'properties': {
    'KeyServer': {'type': 'string'}
  }
};

let jwtDecode = function (options) {
  try {
    let v = new Validator();
    v.validate(options, optionsSchema, {throwError: true});
  } catch (error) {
    console.error('JSON-Schema validation failed: ' + error);
  }

  this._keyServer = options.KeyServer || "https://keyservice.microtronics.com";
  this._client = axios.create({
    baseURL: this._keyServer,
    timeout: options.timeout || 5000,
    httpsAgent: new https.Agent({keepAlive: true})
  });

  this._publicKey = options.publicKey;

  //this option may be set from the outside to avoid the check of the signature part (useful for development purposes)
  this._skipVerification = options.skipVerification || false;
};

jwtDecode.prototype.verifyToken = async function (token) {
  let publicKey = this._publicKey || await requestPublicKey(this._client, token);

  if (!this._skipVerification)
    await verifyToken(token, publicKey);
};

jwtDecode.prototype.verifyRequest = async function (req) {
  const auth = req.get('Authorization').split(' ');
  if (auth[0].toLowerCase() !== 'bearer' || auth.length !== 2)
    throw new Error('The request does not contain a valid JWT token');

  await this.verifyToken(auth[1]);
  req._jwtPayload = JSON.parse(Buffer.from(auth[1].split('.')[1], 'base64').toString());
};

const requestPublicKey = async (client, token) => {
  let payload = JSON.parse(new Buffer(token.split('.')[1], 'base64'));
  let path = '/api/1/keys/' + payload.orig;   // payload.orig = applianceid
  let result = await client.get(path);
  return result.data.PublicKey;
};

const verifyToken = async (token, publicKey) => {
  let pubKey = new sshpk.Key({
    type: 'ed25519',
    parts: [
      {name: 'A', data: Buffer.from(publicKey, 'hex')}
    ]
  });
  let tokenParts = token.split('.');
  let header = JSON.parse(Buffer.from(tokenParts[0], 'base64').toString());
  let alg = /^(ES|RS)512$/.exec(header.alg);
  if (header.typ !== "JWT" || !alg)
    throw new Error('This is not a JWT token');

  let signature = new sshpk.Signature({
    type: (alg[1] === 'ES' ? 'ed25519' : 'rsa'),
    hashAlgo: 'sha512', //we're only going to support sha512
    parts: [
      {name: 'sig', data: Buffer.from(tokenParts[2], 'base64')}
    ]
  });
  let verifier = pubKey.createVerify();
  verifier.update(`${tokenParts[0]}.${tokenParts[1]}`);
  if (verifier.verify(signature)) {
    let payload = JSON.parse(Buffer.from(tokenParts[1], 'base64').toString());
    // eslint-disable-next-line no-prototype-builtins
    if (payload.hasOwnProperty('exp')) {
      let current_date = Math.floor(new Date() / 1000) + (300); // 5 mins offset;
      if (payload.exp < current_date)
        throw new Error('JWT token is expired');
    }
    return true;
  } else {
    throw new Error('JWT signature check failed');
  }
};

module.exports = jwtDecode;
